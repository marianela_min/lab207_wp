<!-- Parameters for this templares are: title: NS &amp; APPS LAB names: is an array with the names of link in the nav bar. footers: array with info for the footer 
members: lista de tuples que contienen nombre y descripcion del miembro.
 -->
 <!DOCTYPE html>
<html>
    <head>
      <?php  echo file_get_contents("html_head.html"); ?>
    </head>
    <body>
      <header>
         <?php echo file_get_contents("navBar.html"); ?>
      </header>



      <main>
        <section id="about">
          <h2>About</h2>
          <div class="container">
           <div class="row justify-content-center">
            <div class="col-10">
              <?php echo file_get_contents("about.html"); ?>
            </div>
           </div>
          </div>
        </section>

      <?php echo file_get_contents("css/sec-divider.html"); ?>
        



        <section id="projects">
          <h2> Projects</h2>
            <div class="container">
                <div class="row">
                <h3 class="col-12">NEIU-SEC</h3>

                <div id="teaching" class="card-projects col-sm-12" >
                    <h4>Methods of Teaching: Implementing Affinity Research Group Model in the Classroom</h4>
                    <p >Student engagement, both inside and outside of the classroom, is a pre-requisite to critical thinking, and Dr. Graciela Perera has adapted the Affinity Research Group (ARG) Model to foster deep student engagement in her computer networking and cybersecurity courses. ARG is a team-based learning strategy for undergraduate research that develops scholarly inquiry skills in students through an intentionally inclusive, cooperative approach. Dr. Perera has adapted the model to ensure her students are active in the research settings she constructs within her courses. Because the approach is overtly supportive and reassuring, students can be challenged and engaged with minimal distractors often present in competitive research environments. Her students are more comfortable to freely engage in the critical thinking activities that are foundational to performing research.</p></div>
                <div id="security" class="card-projects col-sm-12 " >
                    <h4>Network Security</h4>
                    <p >Cyber-security experiments exploring the vulnerabilities of the Internet’s topology<br>Investigating the Internet’s topology is one component towards developing mechanisms that can protect the communication infrastructure underlying our critical systems and applications. We study the feasibility of extracting content providers’ vertex and path properties from snapshots collected by geographically distributed vantage points. Fitting the snapshots to an emulated environment provide excitement and challenge. Internet topology snapshots at the AS-level can be represented as a graph. With the number of ASes on the Internet in the order of tens of thousands, these graphs approach large vertex and edge counts. Emulating even a small section of the topology can be cumbersome. Physical limitations on testbed environments limit us in the size and connectivity of our models. We are constrained in vertex count and degree, the number of edges per vertex. In this paper, we study the graph attributes of snapshots collected and propose a solution on how we can emulate high vertex degrees in an emulated environment with limited degree capabilities. Also, we constructed a proof of concept topology at the Autonomous Systems (AS) level in an emulated environment. This topology shows a Multiple Origin Autonomous System (MOAS) conflict expandable to our snapshots that illustrate our solution.</p></div>
                <div id="algorth" class="card-projects col-sm-12" >
                    <h4>New algorithms for detecting packet anomalies without disrupting network operations</h4>
                    <p >The Internet has developed from an academic-only network of few trusted hosts and users into an essential component of our communication system. Recent changes in Internet’s network infrastructure technology and clean slate design consider security as a critical factor. In particular, core routers are shared network infrastructure with high data rates that transmit much of Internet’s traffic. Detecting anomalies on packets can aid in preventing Distributed Denial of Service (DDoS) attacks directed to the routers themselves and end-systems. We investigate the feasibility of evaluating and tuning the implementation of a filtering mechanism based on the Skyline Algorithm on the GENI (Global Environmental for Network Innovation) testbed. We can use criteria such as packet size, and link congestion to determine a list of packet anomalies.</p></div>
                </div>
             <h3 class="col-xs-12">INFRASTRUCTURE SAFETY</h3>
             <div class="row justify-content-center">
               <div id="bridge" class="col-sm-12" >
                <h4>Monitoring Bridges</h4>
                <p>We seek to apply and test Skyline algorithms (i.e., algorithms that allow efficient evaluation of multiple user preferences) for assessing the health of bridges (i.e., stress) in the Mahoning County. The design and evaluation of the algorithm will be implemented using data collected from a lightweight and reliable wireless sensor network and in a bridge located in the Mahoning County, Ohio.&nbsp;Possible structural metrics will be studied to predict the reliability and safety of bridges.<br>This could potentially save human lives, avoid costly failure, prevent unnecessary reconstructions, and minimize disruptions of traffic. This research will expand the existing CTME research by collecting real bridge data and designing innovative distributed algorithms that can select aggregate results from wireless sensor nodes using multiple criteria. This can allow the structural health assessment and prediction of bridges for safety and longevity.
              </p></div>
             </div>
             <h3>APPS</h3>
             <div class="row">
              <div id="SteamC" class="col-sm-12" >
                <h4>SteamCalc</h4>
               <p>SteamCalc is a chemical engineering application that calculates properties of water steam using IF97 formulation. Valid inputs are pressure and temperature. Output properties are volume, density, entropy, enthalpy, and internal energy. A must have for chemical engineering students. Don't waste your time flipping through pages of steam tables. <a href="https://play.google.com/store/apps/details?id=rek.steam&amp;hl=en">SteamCalc</a></p>
              </div>
           </div>
            </div>
        </section>
<?php echo file_get_contents("css/sec-divider.html"); ?>

 <!--        <section id="publications">
          <h2>Publications</h2>
          <div class="row">
          
          </div>
        </section>
  -->       <section id="members">
           <h2>Collaborators</h2>
           <div class="container">
               <div class="row">
                  <?php echo file_get_contents("members.html"); ?>
               </div>
           </div>
        </section>

<?php echo file_get_contents("css/sec-divider.html"); ?>

      </main>

      <aside class="r-panel" hidden>
      	<section id="news">
          <h2>News</h2>
          <div>
            <p>Esta es la seccion de noticias y eventos que estan programados, es posible colocar un calendario de google para dar mas info.</p>
          </div>
        </section>

      </aside>
    

      <footer id = "contact" >
        <div class="container">
          <div class="row">
            <?php echo file_get_contents("footer.html"); ?>
          </div>
        </div>
      </footer>
   
<div class="copyrights">
             <div class="container">
            <div class="row align-items-strech ">
              <div class="col-6 text-center">
                <span>Designed and developed by Marianela Crissman</span></div>
              <div class="col-6 text-center">
                <span>Copyrights 2018</span></div>
            </div>
          </div>
  
</div>
   
   
   <!-- Do not touch leave this at the very end of the code, right before closing body tag -->
      <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
   </body></html>